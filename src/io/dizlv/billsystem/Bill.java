package io.dizlv.billsystem;

import java.util.Scanner;

/**
 * Entry point for program.
 * Calculates bills for person.
 * @author Dmitrijs Zubriks
 */
public class Bill {

    public static void main(String[] args) {
        Person person = new Person();
        ExtraTariff tariff = new ExtraTariff();

        Scanner in = new Scanner(System.in);

        System.out.println("Type tariff number [1, 2]:");
        int tariffNumber = in.nextInt();

        System.out.println("Type minutes for calls:");
        double minutes = in.nextDouble();

        System.out.println("Type SMS Count: ");
        int sms = in.nextInt();

        // Choosing the tariff.
        switch (tariffNumber) {
            case 1:
                tariff.setCost(20);
                tariff.setFreeCallMinutesCount(200);
                tariff.setFreeSMSCount(150);
                break;
            case 2:
                tariff.setCost(35);
                tariff.setFreeCallMinutesCount(400);
                tariff.setFreeSMSCount(350);
                break;
            default:
                System.out.println("No such tariff.");
                System.exit(-1);
        }

        person.setTotalCallMinutesCount(minutes);
        person.setTotalSMSCount(sms);

        person.addTariff(tariff);

        // Display bill.
        String bill = String.format("Calls:\t%f\nSMS:\t%f\nTotal:\t%f",
                person.getPaymentForCalls(), person.getPaymentForSMS(), person.getTotalPayment());

        System.out.println(tariff.getTariffRepresentation());
        System.out.println(bill);
    }

}
