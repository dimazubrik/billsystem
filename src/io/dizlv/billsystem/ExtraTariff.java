package io.dizlv.billsystem;

/**
 * Extra tariff with free calls and sms.
 * @author Dmitrijs Zubriks
 */
public class ExtraTariff extends Tariff {

    private double cost = 0.0;
    private double freeCallMinutesCount = 0.0;
    private int freeSMSCount = 0;

    public ExtraTariff() {}

    /**
     * Initial values for tariff.
     * @param cost tariff cost.
     * @param freeCallMinutesCount free call minutes count for tariff.
     * @param freeSMSCount free sms count for tariff.
     */
    public ExtraTariff(double cost, double freeCallMinutesCount, int freeSMSCount) {
        this.cost = cost;
        this.freeCallMinutesCount = freeCallMinutesCount;
        this.freeSMSCount = freeSMSCount;
    }

    /**
     * Accessor for cost.
     * @return double value that represents tariff cost.
     */
    public double getCost() {
        return cost;
    }

    /**
     * Accessor for freeCallMinutesCount.
     * @return double value that represents free call minutes count.
     */
    public double getFreeCallMinutesCount() {
        return freeCallMinutesCount;
    }

    /**
     * Accessor for freeSMSCount.
     * @return integer value that represents free sms count.
     */
    public int getFreeSMSCount() {
        return freeSMSCount;
    }

    /**
     * Mutator for cost.
     * @param cost new tariff cost value.
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Mutator for freeCallMinutesCount.
     * @param freeCallMinutesCount new free call minutes count value.
     */
    public void setFreeCallMinutesCount(double freeCallMinutesCount) {
        this.freeCallMinutesCount = freeCallMinutesCount;
    }

    /**
     * Mutator for freeSMSCount.
     * @param freeSMSCount new free sms count value.
     */
    public void setFreeSMSCount(int freeSMSCount) {
        this.freeSMSCount = freeSMSCount;
    }

    /**
     * Formats string with tariff cost, free call minutes and free sms count.
     * @return formatted String.
     */
    public String getTariffRepresentation() {
        return String.format("Tariff Cost:\t%f\nFree Call Minutes:\t%f\nFree SMS Count:\t%d",
                cost, freeCallMinutesCount, freeSMSCount);
    }

}
