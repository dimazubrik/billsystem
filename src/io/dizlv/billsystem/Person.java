package io.dizlv.billsystem;

/**
 * Represents person. Holds different values to compute bills based on tariffs.
 * @author Dmitrijs Zubriks
 */
public class Person {

    private double totalCallMinutesCount = 0;
    private int totalSMSCount = 0;

    private Tariff baseTariff;
    private ExtraTariff extraTariff;

    /**
     * Initializing base tariff for charging money, if there are no other extra tariffs working.
     */
    public Person() {
        baseTariff = new Tariff(0.1, .05);
    }

    /**
     * Add new extra tariff.
     * @param tariff ExtraTariff object with some nice non-chargable calls and messages.
     */
    public void addTariff(ExtraTariff tariff) {
        extraTariff = tariff;
    }

    /**
     * Calculate payment for calls. Based on extra tariff values.
     * @return calculated double value to pay for calls.
     */
    public double getPaymentForCalls() {
        double paidCallMinutesCount = 0;

        if (totalCallMinutesCount > extraTariff.getFreeCallMinutesCount()) {
            paidCallMinutesCount = totalCallMinutesCount - extraTariff.getFreeCallMinutesCount();
        }

        return paidCallMinutesCount * baseTariff.getCallsRate();
    }

    /**
     * Calculate payment for messages. Based on extra tariff values.
     * @return calculated double value to pay for messages.
     */
    public double getPaymentForSMS() {
        int paidSMSCount = 0;

        if (totalSMSCount > extraTariff.getFreeSMSCount()) {
            paidSMSCount = totalSMSCount - extraTariff.getFreeSMSCount();
        }

        return paidSMSCount * baseTariff.getSMSRate();
    }

    /**
     * Calculate total payment (payment for calls + payment for messages + tariff cost)
     * @return calculated double value for all services.
     */
    public double getTotalPayment() {
        return getPaymentForCalls() + getPaymentForSMS() + extraTariff.getCost();
    }

    /**
     * Set new call minutes count.
     * @param totalCallMinutesCount total minutes that were called.
     */
    public void setTotalCallMinutesCount(double totalCallMinutesCount) {
        this.totalCallMinutesCount = totalCallMinutesCount;
    }

    /**
     * Set new messages count.
     * @param totalSMSCount total messages that were sent.
     */
    public void setTotalSMSCount(int totalSMSCount) {
        this.totalSMSCount = totalSMSCount;
    }

}
