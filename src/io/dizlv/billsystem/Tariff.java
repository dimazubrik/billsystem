package io.dizlv.billsystem;

/**
 * Tariff representation. Also used for 'base' tariff that is used without extra tariffs.
 * @author Dmitrijs Zubriks
 */
public class Tariff {

    private double callsRate = 0.0;
    private double smsRate = 0.0;

    public Tariff() {}

    /**
     * Initialize tariff with start-values.
     * @param callsRate rate for every call minute.
     * @param smsRate rate for every message.
     */
    public Tariff(double callsRate, double smsRate) {
        this.callsRate = callsRate;
        this.smsRate = smsRate;
    }

    /**
     * Accessor method for callsRate.
     * @return double value with rate for call.
     */
    public double getCallsRate() {
        return callsRate;
    }

    /**
     * Accessor method for smsRate.
     * @return double value with rate for message.
     */
    public double getSMSRate() {
        return smsRate;
    }

    /**
     * Mutator method for callsRate.
     * @param rate new rate for callsRate.
     */
    public void setCallsRate(double rate) {
        callsRate = rate;
    }

    /**
     * Mutator method for smsRate.
     * @param rate new rate for smsRate.
     */
    public void setSMSRate(double rate) {
        smsRate = rate;
    }

}
